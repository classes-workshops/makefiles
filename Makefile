############################################################
## Copyright 2021
## Author: Luis G. Leon-Vega <lleon95@estudiantec.cr>
############################################################

# Phony defines the common/global targets
.phony: all clean

# It defines where the includes are located
INCLUDES=./include
# It explores src/ and grabs all the .cpp files. It also includes main.cpp
SRCS=$(wildcard src/*.cpp) main.cpp
# It defines the name of the output binary
EXE=main
# It replaces every occurrence of SRCS (all .cpp) and change the extension to
# be .o. For example:
# SRCS=main.cpp file2.cpp -> OBJS=main.o file2.o
OBJS=$(SRCS:.cpp=.o)
# It is just a macro for the compiler
CC=g++
# It defines all the flags for the compiler
# CUSTOM_MESSAGE_FLAG: it is an environment variable that can help you to define
# any flag to insert within the CFLAGS
CFLAGS+=-I$(INCLUDES) -Ofast $(CUSTOM_MESSAGE_FLAG) -std=c++11

# when you do "make", it is executed. The main dependence is $(EXE)
all: $(EXE)

# For building $(EXE), it shall link all the object files. It depends on the
# objects
$(EXE): $(OBJS)
	$(CC) $(OBJS) -o $@ $(CFLAGS)

# This is for building each object file. %.o means that this rule applies to any
# requirement or target .o. %.cpp means that each %.o will depend on a .cpp with
# the same name
%.o: %.cpp
	$(CC) -c -o $@ $< $(CFLAGS)

# This erases everything
clean:
	$(RM) *.o $(EXE) src/*.o

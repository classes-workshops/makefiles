# Makefiles

## Instructions

Please, find some information in:

* [main.cpp](./main.cpp)
* [Makefile](./Makefile)

The idea of this tutorial is to:

* Identify yourself with Makefiles with a simple example
* How to use some wildcards
* How to compile a project with multiple source files and link them altogether.

This project can be used for C/C++/FORTRAN projects without any issue

## Compiling

Building the project with defaults:

```bash
make
```

For passing a custom message:

```bash
/* Using a variable */
CUSTOM_MESSAGE_FLAG='-DMESSAGE="Hola Mundo!"' make -B
/* Redefining the CFLAGS */
CFLAGS='-DMESSAGE="Bonjour Monde!"' make -B
```

Cleaning:

```bash
make clean
```

Author: Luis G Leon-Vega

MIT License 2021

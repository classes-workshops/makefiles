/*
 * Copyright 2021
 * Author: Luis G. Leon-Vega <lleon95@gmail.com>
 * See LICENSE for licensing information
 */

#pragma once

#include <memory>
#include <stdexcept>
#include <string>

enum class Writers {
  Screen,
  File
};

class IWriter {
 public:
  /**
   * Default constructor
   */
  IWriter() {};

  /**
   * Default Destructor
   */
  virtual ~IWriter() {};

  /**
   * Writing method
   */
  virtual void Write(const std::string &msg) = 0;
  /**
   * Overload method for writing in case of requiring a path
   */
  virtual void Write(const std::string &msg, const std::string &path) {
    throw std::runtime_error("Not implemented");
  }

  /**
   * Create a writer
   */
  static std::shared_ptr<IWriter> Factory(const Writers writer);
};

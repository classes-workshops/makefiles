/*
 * Copyright 2021
 * Author: Luis G. Leon-Vega <lleon95@gmail.com>
 * See LICENSE for licensing information
 */

#pragma once

/* This is thanks to the -I./include flag */
#include "writers.hpp"

#include <fstream>
#include <stdexcept>

class FileWriter : public IWriter {
 public:
  FileWriter() : path_{}, is_open_{false} {}
  FileWriter(const std::string &path);

  virtual void Write(const std::string &msg);
  virtual void Write(const std::string &msg, const std::string &path);

  virtual ~FileWriter();
 private:
  std::string path_;
  std::ofstream outfile_;
  bool is_open_;
  bool open();
};

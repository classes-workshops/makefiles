/*
 * Copyright 2021
 * Author: Luis G. Leon-Vega <lleon95@gmail.com>
 * See LICENSE for licensing information
 */

#pragma once

/* This is thanks to the -I./include flag */
#include "writers.hpp"

#include <iostream>

class ScreenWriter : public IWriter {
 public:
  virtual void Write(const std::string &msg);
};

/*
 * Copyright 2021
 * Author: Luis G. Leon-Vega <lleon95@gmail.com>
 * See LICENSE for licensing information
 */

/* This is thanks to the -I./include flag */
#include "screenwriter.hpp"

#include <iostream>

void ScreenWriter::Write(const std::string &msg) {
  std::cout << msg << std::endl;
}
